## NB! This is a dead project! It's not supported anymore, so there is no guarantee that it will work on latest versions of browser.

# Short Description
Extension opens Fisheye Commit Graph view of specified repository in new tab. User needs only insert key word of repository.
It is useful for developers or testers who often need to open commit graphs of different repositories - extension saves clicks and time!

# Download and Install
* **[Install](https://chrome.google.com/webstore/detail/fisheye-commit-graph-open/iedkjhgaoiceaddkolomdfbdelbgipdb "Install")** last version from Google Store

# Questions and Comments
Any questions or comments are welcome! You can write me an e-mail on [irina.ivanova@protonmail.com](irina.ivanova@protonmail.com) or create an issue here.

# Description
Basically extension simply adds your repository code to specified URL and opens it in new tab:
`URL/graph/repository`

Where `URL` and `REPOSITORY` are parameters, that user should specify in Options page (only one time after installation).

After options are specified and saved open extension, insert key word, click Enter and extension will open Commit Graph of required repo. Field for inserting key words has autocomplete function (shows max 5 values).

# Options
* **URL of Fisheye**
* **Repository key-words and codes -** insert key word and repository code in the following format:

   `key-word:REPOSITORY`

   `key-word2:REPOSITORY2`

   Both values are case insensitive.

   Both values can contain all symbols except for : colon.

   Spaces at the start or the end of both values will be trimmed.

# Chrome Tip
You can configure hot-keys for extension in the Google Chrome:
* open the extensions tab - `chrome://extensions`
* link "Configure commands" at the bottom
* choose an extension and type a shortcut
* now You can use it completely without a mouse!

# Posts About fisheyeGraph
* *January 29, 2017* [Profile Page with HTML5 and CSS3](https://ivanova-irina.blogspot.nl/2017/01/profile-page-with-html5-and-css3.html "Profile Page with HTML5 and CSS3")
* *September 17, 2014* [Fisheye Commit Graph Opener v1.3](http://ivanova-irina.blogspot.com/2014/09/fisheye-commit-graph-opener-v13.html "Fisheye Commit Graph Opener v1.3")
* *September 8, 2014* [fisheyeGraph v1.2](http://ivanova-irina.blogspot.com/2014/09/fisheyegraph-v12.html "fisheyeGraph v1.2")
* *September 4, 2014* [fisheyeGraph v1.1](http://ivanova-irina.blogspot.com/2014/09/fisheyegraph-v11.html "fisheyeGraph v1.1")
* *September 3, 2014* [New Chrome Extension fisheyeGraph v1.0](http://ivanova-irina.blogspot.com/2014/09/new-chrome-extension-fisheyegraph-v10.html "New Chrome Extension fisheyeGraph v1.0")
