function save_options() {
	var url=document.getElementById("urlOption").value;
	var repos=document.getElementById("reposOption").value;
    var status=document.getElementById("status");
    var error=document.getElementById("error");
    var borderURL=document.getElementById("urlOption");
    var borderRepos=document.getElementById("reposOption");

    if (url === "" && repos === "") {
        error.textContent="Please fill URL and describe at least one repo";
        borderURL.style.borderColor="#CC0000";
        borderRepos.style.borderColor="#CC0000";
        borderURL.focus();
    } else if (url === "") {
        borderRepos.style.border="";

        error.textContent="Please fill URL";
        borderURL.style.borderColor="#CC0000";
        borderURL.focus();
    } else if (repos === "") {
        borderURL.style.border="";

        error.textContent="Please describe at least one repo";
        borderRepos.style.borderColor="#CC0000";
        borderRepos.focus();
    } else {
        error.innerText="\n";
        borderURL.style.border="";
        borderRepos.style.border="";
        borderURL.focus();
        borderURL.select();

        chrome.storage.sync.set({
            savedUrl: url,
            savedRepos: repos
        }, function () {
            status.textContent="Options are saved";
            setTimeout(function () {
                status.textContent="";
            }, 2000);
        });
    }
}

function restore_options() {
	chrome.storage.sync.get({
		savedUrl: "https://fisheye.atlassian.com",
		savedRepos: "key-word:REPOSITORY\nkey-word2:REPOSITORY2"
	}, function (items) {
		document.getElementById("urlOption").value=items.savedUrl;
		document.getElementById("urlOption").select();
		document.getElementById("reposOption").value=items.savedRepos;
	});
}
document.addEventListener("DOMContentLoaded", restore_options);
document.getElementById("save").addEventListener("click",
	save_options);



var enter=13;

function inputURLListener(e) {
	if (e.keyCode === enter) {
		save_options();
	}
}

function listenInputURL(inputURL) {
	if (inputURL.addEventListener) {
		inputURL.addEventListener("keydown", inputURLListener, false);
	} else if (inputURL.attachEvent) {
		inputURL.attachEvent("keydown", inputURLListener);
	}
}

function listenURL() {
	listenInputURL(document.getElementById("urlOption"));
}

if (window.addEventListener) {
	window.addEventListener("load", listenURL, false);
} else if (window.attachEvent) {
	window.attachEvent("onload", listenURL);
} else {
	document.addEventListener("load", listenURL, false);
}
