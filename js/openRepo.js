function setErrorText(errorText) {
	var divError=document.getElementById("error");
	divError.innerText=errorText + "\n";
}

function returnError(errorText) {
	setErrorText(errorText);
	throw "";
}

var moduleCodes={},
	repo,
	url,
    keyWords=[],
    isError=0;

$(function () {
    chrome.storage.sync.get(function (item) {
        var urlOption=item.savedUrl,
            repos=item.savedRepos,
			reposArray=repos.split("\n");

        for (var i=0; i < reposArray.length; i++) {
            var temp=reposArray[i].split(":");
            temp[0]=removeSpaces(temp[0]);
            keyWords[i]=temp[0];
        }
    });

    $("#repo").autocomplete({
        source: function(request, response) {
            var results=$.ui.autocomplete.filter(keyWords, request.term);
            response(results.slice(0, 5));
        }
    });
});

function setUrl(urlOption) {
	if (urlOption.charAt(urlOption.length - 1) === "/") {
		url=urlOption + "graph/";
	} else {
		url=urlOption + "/graph/";
	}
}

function removeSpaces(string) {
    while (string.charAt(string.length - 1) === " ") {
		string=string.slice(0, string.length - 1);
	}

    if (string.charAt(0) === " ") {
		var temp=string.split(" ");
		string=temp[temp.length - 1];
	}

    return string;
}

function setModuleCodes(reposArray) {
	for (var i=0; i < reposArray.length; i++) {
		var temp=reposArray[i].split(":");

        temp[0]=removeSpaces(temp[0]);
        temp[1]=removeSpaces(temp[1]);

		moduleCodes[temp[0].toLowerCase()]=temp[1];
	}
}

function openWindow() {
	if (moduleCodes[repo] !== undefined) {
		window.open(url + moduleCodes[repo]);
	} else if (repo === "") {
        isError=1;
        returnError("Please insert repo key-word");
    } else {
        isError=1;
		returnError("Can't find repo key-word'"  + repo + "'");
	}
}

function openGraph() {
	chrome.storage.sync.get(function (item) {

		var urlOption=item.savedUrl,
			repos=item.savedRepos,
			reposArray=repos.split("\n");
		repo=document.getElementById("repo").value.toLowerCase();
		setUrl(urlOption);
		setModuleCodes(reposArray);

		openWindow();
	});

    setTimeout(function() {
        if (isError === 0) {
            returnError("Please define repos in Options");
        }
    }, 500);
}

var enter=13;

function inputRepoListener(e) {
	if (e.keyCode === enter) {
		openGraph();
	}
}

function listenInputRepo(inputRepo) {
	if (inputRepo.addEventListener) {
		inputRepo.addEventListener("keydown", inputRepoListener, false);
	} else if (inputRepo.attachEvent) {
		inputRepo.attachEvent("keydown", inputRepoListener);
	}
}

function listenKeys() {
	listenInputRepo(document.getElementById("repo"));
}

if (window.addEventListener) {
	window.addEventListener("load", listenKeys, false);
} else if (window.attachEvent) {
	window.attachEvent("onload", listenKeys);
} else {
	document.addEventListener("load", listenKeys, false);
}
